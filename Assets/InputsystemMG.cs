﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum ControlMode { keyboard = 1, Touch = 2 };
public class InputsystemMG : MonoBehaviour
{


    public ControlMode control;
    public float right, up;
    public bool jump, shoot;
    public GameObject UI;
    public Move2 Move2;
    public GameObject KL;
    public MCGAttack MG;

    // Start is called before the first frame update
    void Start()
    {
        MG = GetComponentInChildren<MCGAttack>();
        if (KL != null)
            KL.GetComponent<KillPlayer>();
        Move2 = GetComponent<Move2>();
    }
    public void Right(float input) { right = input; }
    public void UP(float input) { up = input; }
    public void Jump(bool input)
    {
        jump = input;

    }
    public void Shoot(bool input)
    {
        shoot = input;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (KL.GetComponent<KillPlayer>().isdead == false)
        {
            Move2.Move(right, jump);
        }
        Move2.Flip(right);
        MG.ShootUpdate(shoot, up);
    }
    private void Update()
    {

        if (control == ControlMode.keyboard)
        {
            jump = Input.GetKeyDown(KeyCode.Space);
            right = Input.GetAxis("Horizontal");
            up = Input.GetAxis("Vertical");
            shoot = Input.GetKeyDown(KeyCode.X);
        }
        if (UI != null)
                UI.SetActive(false);
        
        else
        {

            UI.SetActive(true);
        }


    }

}
