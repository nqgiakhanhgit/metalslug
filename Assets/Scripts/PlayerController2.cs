﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController2 : MonoBehaviour
{
    public bool facingright = true;
    public bool isground = true;
    public float vehical = 0;
    public float hori = 0;
    Vector3 LocalScale;

    void Start()
    {
        LocalScale = transform.localScale;
    }
    public void Flip()
    {
        if (hori > 0)
        {

            facingright = true;

        }
        else
        if (hori < 0)
        {
            facingright = false;

        }
        if ((facingright) && (LocalScale.x < 0) || !(facingright) && (LocalScale.x > 0))
        {
            LocalScale.x = LocalScale.x * -1;
        }
        transform.localScale = LocalScale;

    }

    //Update is called once per frame
    void Update()
    {
        vehical = Input.GetAxisRaw("Vertical");
        hori = Input.GetAxisRaw("Horizontal");
    }
}


