﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ground2 : MonoBehaviour
{
    GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        player = gameObject.transform.parent.gameObject;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (player != null)
            if (collision.collider.CompareTag("Ground") || collision.collider.CompareTag("Solider"))
            {
                player.GetComponent<Move2>().isjum = true;
            }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (player != null)
            if (collision.collider.CompareTag("Ground"))
            {
                player.GetComponent<Move2>().isjum = false;
            }
    }
}
