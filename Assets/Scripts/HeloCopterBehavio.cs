﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeloCopterBehavio : MonoBehaviour
{
    int heal = 10;
    public Animator anim;
    public GameObject Bom;
    public GameObject InstantitateBoom;
    private Transform target;
    public float moveSpeed;
    public Rigidbody2D rb;
    public GameObject Sarubia;
    private float timeInstantitate;
    private float MaxTimeInstantitate = 1f;

    private float timeSkill;
    private float MaxTimeSkill = 2f;

    void Start()
    {
        Sarubia.gameObject.SetActive(false);
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
      
        rb = GetComponent<Rigidbody2D>();
  
        moveSpeed = 5;
        anim = GetComponent<Animator>();
        heal = 10;
        
    }
    private void Move()
    {
   
        transform.position = Vector2.MoveTowards(transform.position, new Vector2 (target.position.x,this.transform.position.y), moveSpeed * Time.deltaTime);
    }
    void Instantitate()
    {
        Instantiate(Bom, InstantitateBoom.transform.position, Quaternion.identity);
    }
    void Update()
    {
        //code toi uu
        timeInstantitate += Time.deltaTime;
        if(timeInstantitate >= MaxTimeInstantitate)
        {
            Instantitate();
            timeInstantitate = 0;
            MaxTimeInstantitate = 2;
        }

        timeSkill += Time.deltaTime;
        if (timeSkill >= MaxTimeSkill)
        {
            Skill();
            timeSkill = 0;
            MaxTimeSkill = 3;
        }

        Move();
    }




    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Dan"))
        {
            heal--;
            collision.gameObject.SetActive(false);
        }
        if (heal <= 0)
        {
            anim.SetBool("DIe", true);
            Sarubia.gameObject.SetActive(true);

        }
    }
    public void Gravity()
    {
        rb.GetComponent<Rigidbody2D>().gravityScale = 1;
        this.transform.Translate(Vector2.right * Time.deltaTime * 0.05f);
  
    }

    void Skill()
    {
        for (int i = 0; i < 5; i++)
        {
            Instantitate();
        }
    }
    public void OnDestroy()
    {
        GameObject list = GameObject.FindGameObjectWithTag("InstantitateBoom");
        if(list!=null)
        {

            Destroy(GameObject.FindGameObjectWithTag("InstantitateBoom"));
        }
    }

}
