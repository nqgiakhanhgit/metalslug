﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sarubia : MonoBehaviour
{
    public Animator anim;
    public int heal;
    public GameObject SarubiaBullet;
    public GameObject Trans;
    
    public void SarubiaInstanceBullet ()
    {
   
        Instantiate(SarubiaBullet, Trans.transform.position, Quaternion.identity);

    } 
 
    void Start()
    {
        heal = 20;
        anim = GetComponent<Animator>();
 
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Dan"))
        {
            collision.gameObject.SetActive(false);
            heal--;
        }
        if (heal <= 0 || collision.CompareTag("Right"))
        {
            anim.SetBool("Dead", true);
        }
    }

    
    void DestroySarubia()
    {
        if (Trans!=null )
        Destroy(Trans);
    }    
}
