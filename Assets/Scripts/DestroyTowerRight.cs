﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyTowerRight : MonoBehaviour
{
    public int heal;
    public Animator anim;
    public bool isdestroy;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Dan"))
        {
            collision.gameObject.SetActive(false);
            heal--;

        }
        if (heal <= 0)
        {
            anim.Play("Tower-right_destroyed");
        }
    }
    // Update is called once per frame
    public void DessssTrot()
    {
        Destroy(GameObject.FindGameObjectWithTag("RightTown"));
        isdestroy = true;
    }
}
