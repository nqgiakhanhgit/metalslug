﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Gamecontroller : MonoBehaviour
{
    public GameObject menu;
    void Start()
    {
        Time.timeScale = 1;
        if(menu!=null)
        menu.SetActive(false);
       
    }

    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }
    public void Restar()
    {

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void ExitGame()
    { 
        Application.Quit();
        
    }
    public void ExtoStart()
    {
        SceneManager.LoadScene(0);
    }
    public void EndGame()
    {

        Time.timeScale = 0;
        menu.SetActive(true);
    }
    public void NextSc()
    {
        SceneManager.LoadScene(2);
    }
    public void pre()
    {
        SceneManager.LoadScene(1);
    }
}
