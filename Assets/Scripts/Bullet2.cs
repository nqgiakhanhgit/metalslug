﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet2 : MonoBehaviour
{


    //public Rigidbody2D rb2d;
    //public float X, Y;
    //// Start is called before the first frame update
    //private void OnEnable()
    //{
    //    Invoke("Destroy", 1f);
    //}
    //void Start()
    //{
    //    rb2d = GetComponent<Rigidbody2D>();

    //}


    //// Update is called once per frame
    //void Update()
    //{


    //        rb2d.velocity = new Vector2(X,Y);


    //}
    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    if (collision.gameObject.CompareTag("Ground"))
    //    {
    //        gameObject.SetActive(false);
    //    }
    //}
    ////void OnTriggerExit2D(Collider2D other)
    ////{


    ////        if (gameObject.tag == "Dan")
    ////        {
    ////            gameObject.SetActive(false);
    ////        }
    ////        else
    ////        {
    ////            Destroy(gameObject);
    ////        }
    ////    }?

    //private void Destroy()
    //{
    //    gameObject.SetActive(false);
    //}
    //private void OnDisable()
    //{
    //    CancelInvoke();
    //}
    private GameObject player;
    private SpriteRenderer sprite;
    public PlayerController2 plc;
    Vector3 LocalScale;
    void Start()
    {
        plc = GetComponent<PlayerController2>();
        sprite = GetComponent<SpriteRenderer>();
        player = GameObject.FindGameObjectWithTag("Player");
        LocalScale = transform.localScale;
    }
    private void OnEnable()
    {
        Invoke("Destroy", 1f);
    }
    void BulletTrans()
    {
        Vector3 direct = Vector3.right;
        if ((plc.GetComponent<PlayerController2>().facingright) && (LocalScale.x < 0))
        {
            direct = Vector3.right;
        }
        else
        {
            if (!(plc.GetComponent<PlayerController2>().facingright) && (LocalScale.x > 0))
            {
                direct = -Vector3.right;
                if (sprite != null)
                    sprite.flipX = true;
            }

        }
        this.transform.Translate(direct * 5 * Time.deltaTime);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Ground") || collision.collider.CompareTag("Player"))
        {
            gameObject.SetActive(false);
        }
    }
    private void Flip()
    {
        if (plc.GetComponent<PlayerController2>().hori > 0)
        {

            plc.GetComponent<PlayerController2>().facingright = true;

        }
        else
        if (plc.GetComponent<PlayerController2>().hori < 0)
        {
            plc.GetComponent<PlayerController2>().facingright = false;

        }
        if ((plc.GetComponent<PlayerController2>().facingright) && (LocalScale.x < 0) || !(plc.GetComponent<PlayerController2>().facingright) && (LocalScale.x > 0))
        {
            LocalScale.x = LocalScale.x * -1;
        }

    }
    void Update()
    {
        BulletTrans();
    }
    private void Destroy()
    {
        gameObject.SetActive(false);
    }
    private void OnDisable()
    {
        CancelInvoke();
    }

}



