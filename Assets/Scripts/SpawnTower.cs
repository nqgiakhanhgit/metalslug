﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnTower : MonoBehaviour
{
    public GameObject[] spawnPointTown;
    public GameObject Town;
    public GameObject gamecontroller;

    private float timeSpawnBer;
    private float MaxTimeSpawnBer = 1;

    void Start()
    {       
        gamecontroller.GetComponent<Gamecontroller>();    
    }

    void SpawnRangedomTown()
    {
        int point = Random.Range(0, spawnPointTown.Length);
        if(spawnPointTown[point]!=null)
        Instantiate(Town, spawnPointTown[point].transform.position, Quaternion.identity);
     
    }

    // Update is called once per frame
    void Update()
    {
        //code toi uu
        timeSpawnBer += Time.deltaTime;
        if (timeSpawnBer >= MaxTimeSpawnBer)
        {
            SpawnRangedomTown();
            timeSpawnBer = 0;
            MaxTimeSpawnBer = 2;
        }

        if (spawnPointTown[1] == null && spawnPointTown[0] == null && spawnPointTown[2] == null)
        {
            gamecontroller.GetComponent<Gamecontroller>().EndGame();
        }
    }
}
