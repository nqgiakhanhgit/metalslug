﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMCG : MonoBehaviour
{
    private GameObject player;
    private SpriteRenderer sprite;
    public PlayerControllerMG plc;
    Vector3 LocalScale;
    void Start()
    {
        plc = GetComponent<PlayerControllerMG>();
        sprite = GetComponent<SpriteRenderer>();
        player = GameObject.FindGameObjectWithTag("Player");
        LocalScale = transform.localScale;
    }
    private void OnEnable()
    {
        Invoke("Destroy", 1f);
    }
    void BulletTrans()
    {
        Vector3 direct = Vector3.right;
        if((plc.GetComponent<PlayerControllerMG>().facingright) && (LocalScale.x < 0))
        {
            direct = Vector3.right;
        }
        else
        {
            if (!(plc.GetComponent<PlayerControllerMG>().facingright) && (LocalScale.x > 0))
            {
                direct = -Vector3.right;
                if (sprite != null)
                    sprite.flipX = true;
            }
     
        }
        this.transform.Translate(direct * 5 * Time.deltaTime);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.CompareTag("Ground")|| collision.collider.CompareTag("Player"))
        {
            gameObject.SetActive(false); 
        }
    }
    private void Flip()
    {
        if (plc.GetComponent<PlayerControllerMG>().hori > 0)
        {

            plc.GetComponent<PlayerControllerMG>().facingright = true;

        }
        else
        if (plc.GetComponent<PlayerControllerMG>().hori < 0)
        {
            plc.GetComponent<PlayerControllerMG>().facingright = false;

        }
        if ((plc.GetComponent<PlayerControllerMG>().facingright) && (LocalScale.x < 0) || !(plc.GetComponent<PlayerController2>().facingright) && (LocalScale.x > 0))
        {
            LocalScale.x = LocalScale.x * -1;
        }

    }
    void Update()
    {
        BulletTrans();
    }
    private void Destroy()
    {
        gameObject.SetActive(false);
    }
    private void OnDisable()
    {
        CancelInvoke();
    }

}
