﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MCGAttack : MonoBehaviour
{
    public static int BulletIndex;
    public Animator anim;
    public Transform[] Up;
    public Transform hori;
    public Transform DOwn;
    public float inputve;
    private ObjectPooler OP;
    public KillPlayer KL;
    public AudioClip Dead;
    public AudioClip Fire;
    public GameObject AudioS;
    // Start is called before the first frame update
    void Start()
    {
        AudioS.GetComponent<AudioSource>();
        KL = GetComponent<KillPlayer>();
        anim = GetComponent<Animator>();
        OP = ObjectPooler.SharedInstance;
    }
    public void ShootUp(int animindex)
    {
        Shoot(Up[animindex]);
    }
    private void Shoot(Transform init){

       
        GameObject bullet = OP.GetPooledObject(BulletIndex);
        if (bullet != null)
        {
                bullet.transform.position=init.transform.position;
                bullet.transform.rotation = init.transform.rotation;
                bullet.SetActive(true);   
        }
     
}

    public void DeadSound()
    {


        AudioS.GetComponent<AudioSource>().clip = Dead;
        AudioS.GetComponent<AudioSource>().Play();

    }
    // Update is called once per frame
    public void ShootUpdate(bool input , float up)
    {
        if (KL.GetComponent<KillPlayer>().isdead == false)
        {
            if (Input.GetKeyDown(KeyCode.X))
            {
                AudioS.GetComponent<AudioSource>().clip = Fire;
                AudioS.GetComponent<AudioSource>().Play();
            }


            

            if (input && up == 0)
            {
                Shoot(hori);
                if (anim != null)
                    anim.Play("ShootMCG");

            }
            if (inputve > 0)
            {
                if (anim != null)
                    anim.Play("TopShootMG");
            }

            if (input && up < 0)
            {
                Shoot(DOwn);
                if (anim != null)
                    anim.Play("TopShootDown");
            }

        }
    }
}
