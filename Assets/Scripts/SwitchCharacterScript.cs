﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchCharacterScript : MonoBehaviour {

	// referenses to controlled game objects
	public GameObject avatar1, avatar2;
    public bool cham;
    public GameObject Audios;
    public AudioClip HevyMachinGun;
    public GameObject UI1, UI2;

	// variable contains which avatar is on and active

	// Use this for initialization
	void Start () {
        Audios.GetComponent<AudioSource>();
        // anable first avatar and disable another one
        if (avatar1 != null)
        {
            avatar1.gameObject.SetActive(true);
            UI1.gameObject.SetActive(true);
        }
        if (avatar2 != null)
        {
            avatar2.gameObject.SetActive(false);
            UI2.gameObject.SetActive(false);
        }
	}

	// public method to switch avatars by pressing UI button
	public void SwitchAvatar()
	{

        // processing whichAvatarIsOn variable


        // if the first avatar is on
        if (cham == true)
        {
            avatar1.gameObject.SetActive(false);
            UI1.gameObject.SetActive(false);
            avatar2.gameObject.SetActive(true);
            UI2.gameObject.SetActive(true);
        }

			// disable the first one and anable the second one
		

		// if the second avatar is on
		

			// then the first avatar is on now

			// disable the second one and anable the first one
	
		
			
	}
    private void Update()
    {
        if (cham)
        {
            SwitchAvatar();
        }
    }
    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    if (collision.collider.CompareTag("MachineGun"))
    //    {
    //        cham = true;
    //    }
    //}
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("MachineGun"))
        {
            Audios.GetComponent<AudioSource>().clip = HevyMachinGun;
            Audios.GetComponent<AudioSource>().Play();
            cham = true;
        }
    }

}
