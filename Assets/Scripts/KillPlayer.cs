﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillPlayer : MonoBehaviour
{
    public int heal;
    public Animator anim;
    public bool isdead;
    public GameObject gameObjectControl;
    // Start is called before the first frame update
    void Start()
    {
        gameObjectControl.GetComponent<Gamecontroller>();
        anim = GetComponent<Animator>();
    }
    public void EndGame()
    {
        gameObjectControl.GetComponent<Gamecontroller>().EndGame();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Fire"))
        {
            isdead = true;
            anim.SetBool("Die", true);
            
            
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("hitbox"))
        {
            heal--;
            if (heal <= 0)
            {
                isdead = true;
                anim.SetBool("Die2", true);
            }
        }
        if (collision.gameObject.CompareTag("SarubiaBullet"))
        {
            isdead = true;
            anim.SetBool("Die", true);
        }
    }
   
    // Update is called once per frame
}
