﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Pathfinding;
[RequireComponent(typeof(Rigidbody2D))]

public class TownAi : MonoBehaviour
{
    public bool isDestroy =false;
     GameObject target;
    Animator anim;
    public int heal;
    public float speed = 5f;
    public float rotateSpeed = 200f;

    private Rigidbody2D rb;

    void Start()
    {
        heal = Random.Range(2, 4);
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        target =  GameObject.FindGameObjectWithTag("Player");
    }
    #region Comment
    //Neu cho     if(isDestroy==true)
    // heal = Random.Range(2, 4); vao update thi update lien tuc , Neu cho vao OncollisionEnter2D sẽ bị update heal mỗi lần va chạm !
    // ?
    //private void updateHeal()
    //{
    //    //test gài sự kiện
    //    // Khi test sư kiện thì isdestroy == true ko chạy animation ! 
    //    //Why ? 
    //    //if(isDestroy==true)
    //    //heal = Random.Range(2, 5);
    //}
    #endregion
    void DRR()
    {
        Vector2 direction = (Vector2)target.transform.position - rb.position;

        direction.Normalize();

        float rotateAmount = Vector3.Cross(direction, transform.up).z;

        rb.angularVelocity = -rotateAmount * rotateSpeed;

        rb.velocity = transform.up * speed;
    } 
    void Update()
    {
        DRR();
       IsDestroy();
    }
    void IsDestroy()
    {
        if (isDestroy == true)
        {
            anim.SetBool("Isdestroy", true);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Dan"))
        {

            collision.gameObject.SetActive(false);
            heal--;
        }
        if ((heal <= 0) || collision.gameObject.CompareTag("Player")|| collision.gameObject.CompareTag("Ground"))
        {
            isDestroy = true;

        }


    }
    #region Comment Code Thừa
    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    if (collision.gameObject.CompareTag("Dan"))

    //        collision.gameObject.SetActive(false);
    //    heal--;
    //    if ((heal <= 0) || collision.gameObject.CompareTag("Player"))
    //    {
    //        isDestroy = true;
    //        Debug.Log("DesTroy");

    //    }
    //}
    #endregion
    private void Kill()
    {
        Destroy(gameObject);
        isDestroy = false;
        heal = Random.Range(2, 4);
    }

}
