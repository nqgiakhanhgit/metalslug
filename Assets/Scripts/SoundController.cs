﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour
{
    public AudioClip Dead;
    public AudioClip Fire;
    public AudioSource AudioS;
  
    GameObject obj;
    
    // Start is called before the first frame update
    void Start()
    {
        AudioS = GetComponent<AudioSource>();
    }
    public void DeadSound()
    {
      
    
            AudioS.clip = Dead;
            AudioS.Play();
        
    }
    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.X))
        {
            AudioS.clip = Fire;
            AudioS.Play();
        }
    }
}
