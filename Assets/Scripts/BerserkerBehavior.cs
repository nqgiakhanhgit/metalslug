﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BerserkerBehavior : MonoBehaviour
{
    float speed;
    int heal =2 ;
    public Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        speed = 1f;
        heal = 1;
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Translate(Vector2.left * speed*Time.deltaTime);  
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Dan"))
        {
            heal--;
        
   
        }
        if (heal <= 0)
        {
            anim.SetBool("Die", true);
        }
    }
    public void DesGameObj()
    {
        Destroy(gameObject);
        heal = 2;
    }
}
