﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sarubia_Bullet : MonoBehaviour
{
    public Animator anim;
    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player")|| collision.gameObject.CompareTag("Right")||collision.gameObject.CompareTag("Dan"))
        {
            if (collision.gameObject.CompareTag("Dan"))
            {
                collision.gameObject.SetActive(false);
            }
            anim.SetBool("Bum", true);
        }
    }
    void BulletMove()
    {
        this.transform.Translate(Vector2.left * Time.deltaTime*speed);
    }
    // Update is called once per frame
    void Update()
    {
        BulletMove();
    }
    void Bum()
    {
        gameObject.SetActive(false);
    }
}
