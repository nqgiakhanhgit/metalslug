﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyTownCenter : MonoBehaviour
{
    public int heal;
    public Animator anim;
    public bool isdestroy;
    private void Start()
    {
        anim = GetComponent<Animator>();
    }
    // Start is called before the first frame update
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Dan"))
        {
            collision.gameObject.SetActive(false);
            heal--;
        }
      
        if (heal <= 0)
        {

            anim.Play("Tower-center_destroyed");
        }
    }

    public void Desstroy()
    {
        Destroy(GameObject.FindGameObjectWithTag("CenterTown"));
        isdestroy = true;
    }
    

    // Update is called once per frame
    void Update()
    {
        
    }
}
