﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomHelocopter : MonoBehaviour
{
    public Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }
   
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Player") || collision.collider.CompareTag("Ground"))
        {
            anim.SetBool("No", true);
        }
    }
    public void OnDestroy()
    {
        Destroy(gameObject);
    }
}
