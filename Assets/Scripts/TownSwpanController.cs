﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TownSwpanController : MonoBehaviour
{
    public GameObject Town;
    public GameObject TownLeft;
    public GameObject TowRight;
    public GameObject TownCenter;
    public AudioClip Boss;
    public GameObject AudioS;
    // Start is called before the first frame update
    void Start()
    {
        AudioS.GetComponent<AudioSource>();
        if (Town != null)
            Town.gameObject.SetActive(false);
        if (TownLeft != null)
            TownLeft.gameObject.SetActive(false);
        if (TownCenter != null)
            TownCenter.gameObject.SetActive(false);
        if (TowRight != null)
            TowRight.gameObject.SetActive(false);
    }
    // Update is called once per frame
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            AudioS.GetComponent<AudioSource>().clip = Boss;
            AudioS.GetComponent<AudioSource>().Play();
            if(Town!=null)
                Town.gameObject.SetActive(true);
            if (TownLeft != null)
            TownLeft.gameObject.SetActive(true);
            if (TownCenter != null)
                TownCenter.gameObject.SetActive(true);
            if (TowRight != null)
                TowRight.gameObject.SetActive(true);
        }
    }

}
