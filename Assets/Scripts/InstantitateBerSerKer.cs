﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantitateBerSerKer : MonoBehaviour
{
    public GameObject Ber;
    public GameObject poit;

    private float timeSpawnBer;
    private float MaxTimeSpawnBer = 2f;

    //void Start()
    //{
        //InvokeRepeating("SpawnBer", 2, 4);        
    //}

    private void Update()
    {
        //code toi uu
        timeSpawnBer += Time.deltaTime;
        if (timeSpawnBer >= MaxTimeSpawnBer)
        {
            SpawnBer();
            timeSpawnBer = 0;
            MaxTimeSpawnBer = 4;
        }
    }

    void SpawnBer()
    {
        Instantiate(Ber, poit.transform.position, Quaternion.identity);
    }
}
