﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Move2 : MonoBehaviour
{

    //public GameObject top;
    public ControlMode control;
    public GameObject bottom;
 
    public bool isjum = true;
    [SerializeField]
    private float jum;
    public Rigidbody2D rb;
    public bool facingright;
    Vector3 LocalScale;
    


    // Start is called before the first frame update
    void Start()
    {

        if(rb!=null)
        rb = GetComponentInChildren<Rigidbody2D>();
        //top.GetComponent<Animator>();
        if(bottom!=null)
        bottom.GetComponent<Animator>();
        LocalScale = transform.localScale;
    }

    // Update is called once per frame

    public void Move(float right,bool Jump)
    {
        if (control == ControlMode.keyboard)
            jum = 5;
        else
            jum = 0.5f;
        if (right > 0 || right < 0)
        {
            if(bottom != null)
            bottom.GetComponent<Animator>().Play("BottomRun");
        }
        if (right == 0)
        {
            if (bottom != null)
                bottom.GetComponent<Animator>().Play("BottomIde");
        }
        if (Jump && isjum == true)
        {
            rb.AddForce(new Vector2(right * Time.deltaTime, jum), ForceMode2D.Impulse);
        }
        this.transform.Translate(Vector2.right * right * Time.deltaTime);
    }
    public void Flip(float right )
    {
        if (right>0)
        {

            facingright = true;

        }
        else
       if (right < 0)
        {
            facingright = false;

        }
        if ((facingright) && (LocalScale.x < 0) || !(facingright) && (LocalScale.x > 0))
        {
            LocalScale.x = LocalScale.x * -1;
        }
        transform.localScale = LocalScale;

    }
}
