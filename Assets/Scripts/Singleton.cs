﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Vì Sẽ sử dụng singleton nhiều nên người ta tạo ra để mỗi lần sử dụng chỉ cần kế thừa  là oke 
// Cô đơn chưởng pháp thức thứ 3 
//https://nhathuy7996.blogspot.com/2019/07/co-on-chuong-phap-tu-co-ban-en-nang-cao.html?fbclid=IwAR0JItILHu3FiYj_QGLW3PjK-2Z8HCxCge5_n67P6DMb26rFwOJ2fFFwBIk

public class SingletonClass<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T instan;

    public static T _Instan
    {
        get
        {
            if (instan == null)
            {
                instan = (T)FindObjectOfType(typeof(T));
            }

            if (instan == null)
            {
                GameObject obj = new GameObject();
                instan = obj.AddComponent<T>();
            }
            return instan;

        }
    }
}

