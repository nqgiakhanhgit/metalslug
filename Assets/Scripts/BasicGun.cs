﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicGun : MonoBehaviour
{
    public static int BulletIndex;
    public Animator anim;
    public Transform Up;
    public Transform hori;
    public Transform DOwn;
    public float inputve;
    private ObjectPooler OP;
    public AudioClip Dead;
    public AudioClip Fire;
    public GameObject AudioS;
    // Start is called before the first frame update
    void Start()
    {
        AudioS.GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
        OP = ObjectPooler.SharedInstance;
    }
    private void Shoot(Transform init)
    {
        GameObject bullet = OP.GetPooledObject(BulletIndex);
        if (bullet != null)
        {
            {
                bullet.transform.position = init.transform.position;
                bullet.transform.rotation = init.transform.rotation;
                bullet.SetActive(true);
            }
        }

    }
    public void DeadSound()
    {


        AudioS.GetComponent<AudioSource>().clip = Dead;
        AudioS.GetComponent<AudioSource>().Play();

    }
    public void ShootHori()
    {
        Shoot(hori);
    }
    public void ShootUp()
    {
        Shoot(Up);
    }
    public void ShootDown()
    {
        Shoot(DOwn);
    }

    public void ShootUpdate(float input,bool inp)
    {
        if (inp)
        {
            AudioS.GetComponent<AudioSource>().clip = Fire;
            AudioS.GetComponent<AudioSource>().Play();
        }
        
        if (inp && input == 0)
        {
            anim.Play("Shoot");

        }
        if (inp && input > 0)
        {
            anim.Play("ShootUp");
        }
        if (inp && input < 0)
        {
            anim.Play("ShootDown");
        }
    }

    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.X))
        //{
        //    AudioS.GetComponent<AudioSource>().clip = Fire;
        //    AudioS.GetComponent<AudioSource>().Play();
        //}
        //inputve = Input.GetAxis("Vertical");
        //if(Input.GetKeyDown(KeyCode.X) &&inputve == 0)
        //{
        //    anim.Play("Shoot");

        //}
        //if (Input.GetKeyDown(KeyCode.X) && inputve > 0)
        //{
        //    anim.Play("ShootUp");
        //}
        //if (Input.GetKeyDown(KeyCode.X) &&  inputve < 0)
        //{
        //    anim.Play("ShootDown");
        //}

    }
   
}
