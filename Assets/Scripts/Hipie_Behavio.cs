﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hipie_Behavio : MonoBehaviour
{
    public Animator anim;
    public float speed;
    
    // Start is called before the first frame update
    void Start()
    {
        speed = 8;
        anim = GetComponent<Animator>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
            anim.SetBool("MCGinstantitate", true);
        else
        {
            if(collision.CompareTag("END"))
            DestroyHipie();
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            anim.SetBool("HipieRun", true);
            
        }
    }
    public void HipieRun()
    {
        this.transform.Translate(Vector2.right * speed * Time.deltaTime);
    }
    // Update is called once per frame
    void DestroyHipie()
    {
        Destroy(gameObject);
    }    
}
