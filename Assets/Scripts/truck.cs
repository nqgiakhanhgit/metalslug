﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class truck : MonoBehaviour
{
   public Animator anim;
   public int heal;
    public GameObject Trans;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        heal = 10;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Dan"))
        {
            heal--; 
            
                collision.gameObject.SetActive(false);
            
        }
        if (heal <= 0)
        {
            anim.SetBool("Die", true);
        }
    }
    public void DesTruck()
    {
        if(Trans!=null)
        Destroy(Trans);
    }
}
